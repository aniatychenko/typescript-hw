import { callApi } from '../helpers/apiHelper';
import IFighter from '../interfaces/fighterInterface';

class FighterService  {
  async getFighters():Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return <IFighter[]>apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighter>{
    try {
      const endpoint = `details/fighter/${id}.json`;
      const fighter = await callApi(endpoint, 'GET');
      
      return <IFighter>fighter;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
