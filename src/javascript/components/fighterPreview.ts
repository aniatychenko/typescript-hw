import { createElement } from '../helpers/domHelper';
import IFighter from '../interfaces/fighterInterface';

export function createFighterPreview(fighter: IFighter, position: "right" | "left") {
  if (!fighter) {
    return '';
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  let fighterElements: HTMLElement[]= [];
  Object.keys(fighterToFighterPreviewInfo(fighter)).forEach((key) => {
    (key === 'source') ? fighterElements.push(createFighterImage(fighter)) : fighterElements.push(createParam(key, fighter[key as keyof IFighter]))
  });
  fighterElement.append(...fighterElements);
  return fighterElement;
}

function fighterToFighterPreviewInfo({ name, health, attack, defense, source }:IFighter) {
  return {
    source,
    name,
    health,
    attack,
    defense
  }
}

function createParam(paramName: string, param: string | number) {
  const paramElement = createElement({ tagName: 'span', className: 'fighter-preview___info' });
  paramElement.innerText = `${paramName} : ${param}`;

  return paramElement;
}
export function createFighterImage(fighter: IFighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}