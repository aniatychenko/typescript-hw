import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import IFighter from '../interfaces/fighterInterface';
import IFightersSelector from '../interfaces/fighterSelectors';

export function createFightersSelector(): IFightersSelector {
  let selectedFighters: IFighter[] = [];

  return async (event: Event, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);  
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne || fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo || fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

let fighterDetailsMap: Map<string, IFighter> = new Map();

export async function getFighterInfo(fighterId: string) {
  let fighter: IFighter;
  if (fighterDetailsMap.has(fighterId)){
    fighter = fighterDetailsMap.get(fighterId);
  }  
  else {
    fighter = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighter);
  }
  return fighter;
}

function renderSelectedFighters(selectedFighters: IFighter[]) {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighter[]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({
    tagName: 'div',
    className: 'preview-container___versus-block'
  });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: {
      src: versusImg
    },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighter[]) {
  renderArena(selectedFighters);
}