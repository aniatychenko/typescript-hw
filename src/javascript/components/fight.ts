import { controls } from '../../constants/controls';
import { COOLDOWN_TIME } from '../../constants/coolDown';
import IFighter from '../interfaces/fighterInterface';

export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
  return new Promise((resolve) => {
    const maxFirstFighterHealth = firstFighter.health;
    const maxSecondFighterHealth = secondFighter.health;
    const command: Set<string> = new Set();
    let isEnabledfirstCriticalHit = true;
    let isEnabledsecondCriticalHit = true;

    const keyUpHandler = (event: KeyboardEvent) => command.delete(event.code);

    const keyDownHandler = (event: KeyboardEvent) => {
      command.add(event.code);

      if (event.code === controls.PlayerOneAttack) {
        const isAttackerBlocking = command.has(controls.PlayerOneBlock);
        const isDefenderBlocking = command.has(controls.PlayerTwoBlock);
        fighterAttack(firstFighter, secondFighter, isAttackerBlocking, isDefenderBlocking);
        reduceHealthIndicator(secondFighter, 'right', maxSecondFighterHealth);
      }

      if (event.code === controls.PlayerTwoAttack) {
        const isAttackerBlocking = command.has(controls.PlayerTwoBlock);
        const isDefenderBlocking = command.has(controls.PlayerOneBlock);
        fighterAttack(secondFighter, firstFighter, isAttackerBlocking, isDefenderBlocking);
        reduceHealthIndicator(firstFighter, 'left', maxFirstFighterHealth);
      }

      const isPlayerOneCriticalHitCombination = controls.PlayerOneCriticalHitCombination.every(key => command.has(key));
      const isPlayerTwoCriticalHitCombination = controls.PlayerTwoCriticalHitCombination.every(key => command.has(key));

      if (isPlayerOneCriticalHitCombination && isEnabledfirstCriticalHit) {
        isEnabledfirstCriticalHit = false;
        getCriticalDamage(firstFighter, secondFighter);
        reduceHealthIndicator(secondFighter, 'right', maxSecondFighterHealth);

        setTimeout(() => { isEnabledfirstCriticalHit = true }, COOLDOWN_TIME);
      }

      if (isPlayerTwoCriticalHitCombination && isEnabledsecondCriticalHit) {
        isEnabledsecondCriticalHit = false;
        getCriticalDamage(secondFighter, firstFighter);
        reduceHealthIndicator(firstFighter, 'left', maxFirstFighterHealth);

        setTimeout(() => { isEnabledsecondCriticalHit = true }, COOLDOWN_TIME);
      }

      if (firstFighter.health <= 0) { resolve(secondFighter); }
      else if (secondFighter.health <= 0) { resolve(firstFighter); }
    };

    const fighterAttack = (attacker: IFighter, defender: IFighter, isAttackerBlocking: boolean, isDefenderBlocking: boolean) => {
      if (!isAttackerBlocking) {
        const damage = isDefenderBlocking ? 0 : getDamage(attacker, defender);
        defender.health -= damage;
      }
    }

    const reduceHealthIndicator = (defender: IFighter, position: "right" | "left", maxHealth: number) => {
      const newWidth = defender.health * 100 / maxHealth;
      const div = document.getElementById(`${position}-fighter-indicator`);
      div.style.width = newWidth + '%';
    }

    const getCriticalDamage = (attacker: IFighter, defender: IFighter) => {
      const damage = attacker.attack * 2;
      defender.health -= damage;
    }

    document.addEventListener('keyup', keyUpHandler);
    document.addEventListener('keydown', keyDownHandler);
  });
}


export function getDamage(attacker: IFighter, defender: IFighter) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(damage, 0);
}

export function getHitPower(fighter: IFighter) {
  let criticalHitChance = Math.random() * (2 - 1) + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: IFighter) {
  let dodgeChance = Math.random() * (2 - 1) + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}