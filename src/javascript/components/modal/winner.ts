import { showModal } from './modal'
import App from '../../app';
import { createFighterImage } from '../fighterPreview'
import IFighter from '../../interfaces/fighterInterface';

export function showWinnerModal(fighter: IFighter) {
  showModal({
    title: `${fighter.name} is a winner!`,
    bodyElement: createFighterImage(fighter),
    onClose: () => {
      let root = document.getElementById('root');
      root.innerHTML = '';
      new App();
    }
  })
}