interface IAtrributes{
  [key: string]: string;
}
interface ICreateElemets{
  tagName: string;
  className?: string;
  attributes?: IAtrributes;
}
export function createElement({ tagName, className, attributes = {} }: ICreateElemets): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
