interface IFightersSelector{
    (event : Event, fighterId : string): void;
}
export default IFightersSelector;